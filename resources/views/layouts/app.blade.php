<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>{{ config('app.name') }}</title>

        <!-- App styles -->
        <link rel="stylesheet" href="/css/app.css" type="text/css">

        <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body data-ma-theme="green">
    <main class="main" id="app">
        @include('partials.loader')
        @include('partials.header')

        @include('partials.sidebar')

        <section class="content">
            <div class="content__inner">
                @yield('content')
            </div>
        </section>
    </main>

    @include('partials.ie')

    <!-- App functions and actions -->
    @yield('scripts')
    <script src="/js/app.js"></script>
</body>
</html>