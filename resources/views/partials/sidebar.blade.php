<aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="{{ Gravatar::src(Auth::user()->email) }}" alt="">
                <div>
                    @if (Auth::check())
                        <div class="user__name">{{ Auth::user()->name }}</div>
                        <div class="user__email">{{ Auth::user()->email }}</div>
                    @endif
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="">View Profile</a>
                <a class="dropdown-item" href="">Settings</a>
                <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
            </div>
        </div>

        <ul class="navigation">
            <li class="{{ Request::is('home') ? 'navigation__active' : 'navigation' }}">
                <a href="/home"><i class="zmdi zmdi-home"></i> Home</a>
            </li>
        </ul>
    </div>
</aside>
