@extends('layouts.guest')

@section('content')
    <div class="login">
        <!-- Login -->
        <div class="login__block active" id="l-login">
            <div class="login__block__header">
                <i class="zmdi zmdi-account-circle"></i>
                Hi there! Please Sign in
                <div class="actions actions--inverse login__block__actions">
                </div>
            </div>
            <div class="login__block__body">
                <form action="login" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group form-group--float form-group--centered">
                        @if ( count($errors->get('email')) > 0 )
                            <input name="email" type="email"  class="form-control is-invalid">
                            <div class="invalid-feedback">
                                {{ $errors->get('email')[0] }}
                            </div>
                        @else
                            <input name="email" type="email"  class="form-control">
                        @endif
                        <label>Email Address</label>
                        <i class="form-group__bar"></i>
                    </div>

                    <div class="form-group form-group--float form-group--centered">

                        @if ( count($errors->get('password')) > 0 )
                            <input type="password" class="form-control is-invalid" name="password">
                            <div class="invalid-feedback">
                                {{ $errors->get('password')[0] }}
                            </div>
                        @else
                            <input type="password" class="form-control" name="password">
                        @endif
                        <label>Password</label>
                        <i class="form-group__bar"></i>
                    </div>

                    <button type="submit" id="loginButton" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-long-arrow-right"></i></button>
                    <a href="/redirect/google"><button type="button" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-google"></i></button></a>
                </form>
            </div>
            @if ( count($errors->get("groups")) )
                <div class="alert alert-danger">
                    <h6 class="text-white">{{ $errors->first() }}</h6>
                </div>
            @endif
        </div>

    </div>
@endsection