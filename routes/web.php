<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/login');
});

Route::get('/redirect/{provider}', 'Auth\LoginController@redirectToProvider')->name('google.login');
Route::get('/callback/{provider}', 'Auth\LoginController@handleProviderCallback')->name('google.callback');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/logout', function() {
        Auth::logout();
        return redirect('/login');
    });
});
