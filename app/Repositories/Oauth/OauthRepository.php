<?php


namespace App\Repositories\Oauth;


use App\Models\LinkedSocialAccount;
use App\Repositories\BaseRepository;
use Laravel\Socialite\Contracts\User as ProviderUser;

class OauthRepository extends BaseRepository implements OauthRepositoryInterface
{
    public function __construct(LinkedSocialAccount $model)
    {
        parent::__construct($model);
    }

    /**
     * @param ProviderUser $providerUser
     * @param $provider
     * @return LinkedSocialAccount
     */
    public function getByProviderName(ProviderUser $providerUser, $provider)
    {
        return $this->model->where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();
    }
}