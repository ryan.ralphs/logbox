<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\OauthService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $oauthService;

    /**
     * Create a new controller instance.
     *
     * @param OauthService $oauthService
     */
    public function __construct(OauthService $oauthService)
    {
        $this->middleware('guest')->except('logout');
        $this->oauthService = $oauthService;
    }

    public function redirectToProvider($provider)
    {
        return $this->oauthService->redirect($provider);
    }

    public function handleProviderCallback($provider)
    {
        return $this->oauthService->callback($provider);
    }
}